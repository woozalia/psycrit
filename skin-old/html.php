<?php

/**
 * @todo document
 * @ingroup Skins
 */
class PsyCritTemplate extends BaseTemplate {

	/*----
      PURPOSE: Template filter callback for PsyCrit skin.
        Heavily based on MonoBook
      HISTORY:
        2020-09-01 updated for MW 1.34.2: removed wfSuppressWarnings()
    */
	function execute() {
		// Suppress warnings to prevent notices about missing indexes in $this->data
		#wfSuppressWarnings();

		$this->data['sidebar']['SEARCH'] = FALSE;	// don't display searchbox in sidebar

		$this->html( 'headelement' );

/*		echo Html::element( 'table', array(
		  'width'	=> '100%',
		  'class'	=> 'topbanner',
		  'style'	=> "background-image: url($wgxBanner);" )
		  ); */
// This section displays the logo banner

		global $wgxBanner;
		$cssBanner = "background-image: url($wgxBanner) no-repeat;";
		$htImgBanner = '<img src="'.$wgxBanner.'">';
		$htSearch = $this->searchBox();

		echo <<<__END__
<table width=100% class=topbanner><tr><td style="background: #000; width: 100%;">$htImgBanner

<!-- SEARCHBOX -->
	<table align=right><tr><td>
	$htSearch
	</td></tr></table>

<!-- TOP BANNER -->
__END__;
#echo 'ATTRIBS: <pre>'.print_r(Linker::tooltipAndAccesskeyAttribs('p-logo'),TRUE).'</pre>';
        $arToolTip = Linker::tooltipAndAccesskeyAttribs('p-logo');
        
        /* 2020-08-03 this now sticks text in the middle of the banner, which is not helpful
		echo Html::element(
          'a',
          array(
            'href' => $this->data['nav_urls']['mainpage']['href'],
            //'style' => "background-image: url($wgxBanner);"
            ),
            $arToolTip['title'].'BLARG'
          );
        */
?>
</td></tr></table>

<?php /* =================== standard rendering begins =========================== */ ?>

<div id="globalWrapper">
<div id="column-content"><div id="content">
	<a id="top"></a>
	<?php if($this->data['sitenotice']) { ?><div id="siteNotice"><?php $this->html('sitenotice') ?></div><?php } ?>

	<h1 id="firstHeading" class="firstHeading"><span dir="auto"><?php $this->html('title') ?></span></h1>
	<div id="bodyContent" class="mw-body">
		<div id="siteSub"><?php $this->msg('tagline') ?></div>
		<div id="contentSub"<?php $this->html('userlangattributes') ?>><?php $this->html('subtitle') ?></div>
<?php if($this->data['undelete']) { ?>
		<div id="contentSub2"><?php $this->html('undelete') ?></div>
<?php } ?><?php if($this->data['newtalk'] ) { ?>
		<div class="usermessage"><?php $this->html('newtalk')  ?></div>
<?php } ?><?php if($this->data['showjumplinks']) { ?>
		<div id="jump-to-nav" class="mw-jump"><?php $this->msg('jumpto') ?> <a href="#column-one"><?php $this->msg('jumptonavigation') ?></a>, <a href="#searchInput"><?php $this->msg('jumptosearch') ?></a></div>
<?php } ?>
		<!-- start content -->
<?php $this->html('bodytext') ?>
		<?php if($this->data['catlinks']) { $this->html('catlinks'); } ?>
		<!-- end content -->
		<?php if($this->data['dataAfterContent']) { $this->html ('dataAfterContent'); } ?>
		<div class="visualClear"></div>
	</div>
</div></div>
<div id="column-one"<?php $this->html('userlangattributes')  ?>>
<?php $this->cactions(); ?>
	<div class="portlet" id="p-personal">
		<h5><?php $this->msg('personaltools') ?></h5>
		<div class="pBody">
			<ul<?php $this->html('userlangattributes') ?>>
<?php
            foreach($this->getPersonalTools() as $key => $item) {
				echo $this->makeListItem($key, $item);
            } ?>
			</ul>
		</div>
	</div>
	<a href="/"><div class="portlet" id="p-logo">
<?php
/* don't show the logo in this skin
			echo Html::element( 'a', array(
				'href' => $this->data['nav_urls']['mainpage']['href'],
				'style' => "background-image: url({$this->data['logopath']});" )
				+ Linker::tooltipAndAccesskeyAttribs('p-logo') ); 
*/
?>

	</div></a>
<?php
	$arSideBar = $this->data['sidebar'];
	$this->renderPortals( $arSideBar );
?>
</div><!-- end of the left (by default at least) column -->
<div class="visualClear"></div>
<?php
	$validFooterIcons = $this->getFooterIcons( "icononly" );
	$validFooterLinks = $this->getFooterLinks( "flat" ); // Additional footer links

	if ( count( $validFooterIcons ) + count( $validFooterLinks ) > 0 ) { ?>
<div id="footer"<?php $this->html('userlangattributes') ?>>
<?php
		$footerEnd = '</div>';
	} else {
		$footerEnd = '';
	}
	foreach ( $validFooterIcons as $blockName => $footerIcons ) { ?>
	<div id="f-<?php echo htmlspecialchars($blockName); ?>ico">
<?php foreach ( $footerIcons as $icon ) { ?>
		<?php echo $this->getSkin()->makeFooterIcon( $icon ); ?>

<?php }
?>
	</div>
<?php }

		if ( count( $validFooterLinks ) > 0 ) {
?>	<ul id="f-list">
<?php
			foreach( $validFooterLinks as $aLink ) { ?>
		<li id="<?php echo $aLink ?>"><?php $this->html($aLink) ?></li>
<?php
			}
?>
	</ul>
<?php	}
echo $footerEnd;
?>

</div>
<?php
		$this->printTrail();
		echo Html::closeElement( 'body' );
		echo Html::closeElement( 'html' );
		#wfRestoreWarnings();
	} // end of execute() method

	/*************************************************************************************************/

	protected function renderPortals( $sidebar ) {
		if ( !isset( $sidebar['SEARCH'] ) ) $sidebar['SEARCH'] = true;
		if ( !isset( $sidebar['TOOLBOX'] ) ) $sidebar['TOOLBOX'] = true;
		if ( !isset( $sidebar['LANGUAGES'] ) ) $sidebar['LANGUAGES'] = true;

		foreach( $sidebar as $boxName => $content ) {
			if ( $content === false )
				continue;

			if ( $boxName == 'SEARCH' ) {
				$this->searchBox();
			} elseif ( $boxName == 'TOOLBOX' ) {
				$this->toolbox();
			} elseif ( $boxName == 'LANGUAGES' ) {
				$this->languageBox();
			} else {
				$this->customBox( $boxName, $content );
			}
		}
	}

	function searchBox() {
		global $wgUseTwoButtonsSearchForm;
		global $wgScript;
		
		$htLabel = $this->getMsg('search');
		//$htAction = $this->text('wgScript');
		$htAction = $wgScript;
		//$htTitle = $this->text('searchtitle');
		$htTitle = $this->getMsg('searchtitle');

		$htInput = $this->makeSearchInput(array( "id" => "searchInput" ));
		$htBtns = $this->makeSearchButton("go", array( "id" => "searchGoButton", "class" => "searchButton" ));
		if ($wgUseTwoButtonsSearchForm) {
		    $htBtns .= '&#160;'
		      .$this->makeSearchButton("fulltext", array( "id" => "mw-searchButton", "class" => "searchButton" ));
		} else {
		    $htBtns .= '<div><a href="'
		      //.$this->text('searchaction')
		      .$this->getMsg('searchaction')
		      .'" rel="search">'
		      .$this->getMsg('powersearch-legend')
		      .'</a></div>';
		}
		
		$out = <<<__END__
	<div id="p-search" class="portlet">
		<h5><label for="searchInput">$htLabel</label></h5>
		<div id="searchBody" class="pBody">
			<form action="$htAction" id="searchform">
				<input type='hidden' name="title" value="$htTitle"/>
				$htInput
				$htBtns
			</form>
		</div>
	</div>
__END__;
	    return $out;
	}

	/**
	 * Prints the cactions bar.
	 * Shared between MonoBook and Modern
	 */
	function cactions() {
?>
	<div id="p-cactions" class="portlet">
		<h5><?php $this->msg('views') ?></h5>
		<div class="pBody">
			<ul><?php
				foreach($this->data['content_actions'] as $key => $tab) {
					echo '
				' . $this->makeListItem( $key, $tab );
				} ?>

			</ul>
		</div>
	</div>
<?php
	}
	/*************************************************************************************************/
	function toolbox() {
?>
	<div class="portlet" id="p-tb">
		<h5><?php $this->msg('toolbox') ?></h5>
		<div class="pBody">
			<ul>
<?php
		foreach ( $this->getToolbox() as $key => $tbitem ) { ?>
				<?php echo $this->makeListItem($key, $tbitem); ?>

<?php
		}
		#wfRunHooks( 'MonoBookTemplateToolboxEnd', array( &$this ) );
		#wfRunHooks( 'SkinTemplateToolboxEnd', array( &$this, true ) );
?>
			</ul>
		</div>
	</div>
<?php
	}

	/*************************************************************************************************/
	function languageBox() {
		if( $this->data['language_urls'] ) {
?>
	<div id="p-lang" class="portlet">
		<h5<?php $this->html('userlangattributes') ?>><?php $this->msg('otherlanguages') ?></h5>
		<div class="pBody">
			<ul>
<?php		foreach($this->data['language_urls'] as $key => $langlink) { ?>
				<?php echo $this->makeListItem($key, $langlink); ?>

<?php		} ?>
			</ul>
		</div>
	</div>
<?php
		}
	}

	/*************************************************************************************************/
	function customBox( $bar, $cont ) {
		$portletAttribs = array( 'class' => 'generated-sidebar portlet', 'id' => Sanitizer::escapeId( "p-$bar" ) );
		$tooltip = Linker::titleAttrib( "p-$bar" );
		if ( $tooltip !== false ) {
			$portletAttribs['title'] = $tooltip;
		}
		echo '	' . Html::openElement( 'div', $portletAttribs );
?>

		<h5><?php $msg = wfMessage( $bar ); echo htmlspecialchars( $msg->exists() ? $msg->text() : $bar ); ?></h5>
		<div class='pBody'>
<?php   if ( is_array( $cont ) ) { ?>
			<ul>
<?php 			foreach($cont as $key => $val) { ?>
				<?php echo $this->makeListItem($key, $val); ?>

<?php			} ?>
			</ul>
<?php   } else {
			# allow raw HTML block to be defined by extensions
			print $cont;
		}
?>
		</div>
	</div>
<?php
	}
} // end of class
