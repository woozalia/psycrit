<?php namespace psycrit;
/**
 * MonoBook nouveau
 *
 * Translated from gwicke's previous TAL template version to remove
 * dependency on PHPTAL.
 *
 * @todo document
 * @file
 * @ingroup Skins
 */

if ( !defined( 'MEDIAWIKI' ) ) {
	die( 'This is an extension to the MediaWiki package and cannot be run standalone.' );
}

$wgExtensionCredits['skin'][] = array(
	'path' => __FILE__,
	'name' => 'PsyCrit', // name as shown under [[Special:Version]]
	//'namemsg' => 'psycrit', // used since MW 1.24, see the section on "Localisation messages" below
	'version' => '1.2',
	//'url' => 'https://www.mediawiki.org/wiki/Skin:FooBar',
	'author' => '[https://wooz.dev/User:Woozle Woozalia Staddon]',
	//'descriptionmsg' => 'foobar-desc', // see the section on "Localisation messages" below
	'license' => 'GPL-2.0+',
);

$wgValidSkinNames['psycrit'] = 'PsyCrit';
 
$wgAutoloadClasses['SkinPsyCrit'] = __DIR__ . '/skin.php';
$wgAutoloadClasses['PsyCritTemplate'] = __DIR__ . '/html.php';

$wgMessagesDirs['PsyCrit'] = __DIR__ . '/i18n';

/*::::
  PURPOSE: configuration settings for the PsyCrit skin
  THINKING: 2022-03-25 by default, this will assume StaticWebPath is just the "static/" folder
    within the skin repo -- but this can be overridden if you want to protect the skin
    code from being accessed via web..
*/
class csConfig {

    /*----
      PURPOSE: Set web path to folder to PsyCrit skin (all files)
      NOTES:
        2022-03-25 All this does for now is calculate/set the default StaticWebPath.
          ...and I haven't tested it.
    */
    static public function SetSelfWebPath(string $wp) {
        self::SetStaticWebPath($wp.'/static');
    }
  
    // web folder for skin static files (CSS, kept with skin repo)
  
    static private $wpStatic;
    static public function SetStaticWebPath(string $wp) { self::$wpStatic = $wp; }
    static public function GetStaticWebPath() : string { return self::$wpStatic; }
    
    // web spec for banner image (LATER: maybe this should go in the static folder?)
    
    static private $wsBanner;
    static public function SetBannerWebSpec(string $ws) { self::$wsBanner = $ws; }
    static public function GetBannerWebSpec() : string { return self::$wsBanner; }
    
}

#define('KWP_SKIN_PSYCRIT',KWP_CUSTOM_SKIN.'/psycrit');
#define('KWP_SKIN_PSYCRIT_STATIC',KWP_SKIN_PSYCRIT.'/static');
