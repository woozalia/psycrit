<?php

/*
  PURPOSE: Template class for PsyCrit skin
    * Inherits main code from SkinTemplate, sets the CSS and template filter.
  HISTORY:
    2020-09-01 updated to use local copies of IE?0Fixes.css, as I had apparently intended originally
*/
class SkinPsyCrit extends SkinTemplate {
	/** Using monobook. */
	var $skinname = 'psycrit', $stylename = 'psycrit',
		$template = 'PsyCritTemplate', $useHeadElement = true;

	/**
	 * @param $out OutputPage
	 */
	function setupSkinUserCss( OutputPage $out ) {
		global $wgHandheldStyle;
		
		parent::setupSkinUserCss( $out );

		$out->addStyle( KWP_SKIN_PSYCRIT_STATIC.'/main.css', 'screen' );

		// Ugh. Can't do this properly because $wgHandheldStyle may be a URL
		if( $wgHandheldStyle ) {
			// Currently in testing... try 'chick/main.css'
			$out->addStyle( $wgHandheldStyle, 'handheld' );
		}

		// TODO: Migrate all of these
		$out->addStyle( KWP_SKIN_PSYCRIT_STATIC.'/IE60Fixes.css', 'screen', 'IE 6' );
		$out->addStyle( KWP_SKIN_PSYCRIT_STATIC.'/IE70Fixes.css', 'screen', 'IE 7' );
	}
}
